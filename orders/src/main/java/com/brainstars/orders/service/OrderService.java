package com.brainstars.orders.service;

import com.brainstars.orders.models.OrderModel;

public interface OrderService {
    void save(OrderModel orderModel);
}
