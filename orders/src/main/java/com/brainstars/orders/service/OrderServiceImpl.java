package com.brainstars.orders.service;

import com.brainstars.orders.OrderRepository;
import com.brainstars.orders.domain.Order;
import com.brainstars.orders.models.OrderModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public void save(OrderModel orderModel) {
        Order order = mapper.convertValue(orderModel, Order.class);
        orderRepository.save(order);
    }
}
