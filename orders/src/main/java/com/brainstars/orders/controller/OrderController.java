package com.brainstars.orders.controller;

import com.brainstars.orders.config.RabbitMQConfig;
import com.brainstars.orders.models.OrderModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("rest-api/orders")
public class OrderController {
    private final RabbitTemplate rabbitTemplate;

    @ApiOperation(value = "Adds a record with made orders.")
    @PostMapping
    public ResponseEntity<String> addOrder(
            @Parameter(description = "a Order")
            @RequestBody OrderModel orderModel)
    {
        rabbitTemplate.convertAndSend(RabbitMQConfig.Q_NAME,orderModel);

        return ResponseEntity.ok("Successfully executed operation.");
    }
}
