package com.brainstars.orders.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    public static final String Q_NAME = "ordersQueue";

    @Bean
    public Queue rabbitQueue(){
        return new Queue(Q_NAME, false);
    }
}
