package com.brainstars.orders.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Details about OrderModel.")
public class OrderModel implements Serializable {

    @ApiModelProperty("The order identifier.")
    private Long productId;

    @ApiModelProperty("The amount of made order.")
    private Long quantity;
}
