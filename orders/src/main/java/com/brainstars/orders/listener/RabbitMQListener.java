package com.brainstars.orders.listener;

import com.brainstars.orders.config.RabbitMQConfig;
import com.brainstars.orders.models.OrderModel;
import com.brainstars.orders.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RabbitMQListener {

    private final OrderService orderService;

    @RabbitListener(queues = RabbitMQConfig.Q_NAME)
    public void listen(OrderModel orderModel) {
        orderService.save(orderModel);
    }
}
